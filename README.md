# Trabajo de clustering: Análisis de la renta en los municipios españoles mediante Mapas Auto-organizados #

Repositorio del trabajo de clustering de la asignatura de Diseño de Sistemas Inteligentes del Máster de Ingeniería Informática.

Autores: Pablo León Alcaide y Marcos Ludeña Triviño.

Puede consultar vídeos explicativos del trabajo en: https://youtu.be/iF1YCzUZdUU y https://youtu.be/MAIjnL_AJfw. Aunque también le recomendamos consultar el notebook a partir del cual se construye todo el proyecto https://bitbucket.org/PabloLe23/sistemas_inteligentes/src/3cc262e7039e0506a11e99c2ffaa4754ad3dd4c4/clustering/notebook.ipynb?at=master.


# Trabajo de predicción: LSTMs para la predicción de casos de dengue en Iquitos y San Juan #

Repositorio del trabajo de predicción de la asignatura de Diseño de Sistemas Inteligentes del Máster de Ingeniería Informática.

Autores: Pablo León Alcaide y Marcos Ludeña Triviño.

Usuarios de DrivenData: https://www.drivendata.org/users/pabloleon/ y https://www.drivendata.org/users/Marcos21l/.


