results = []

def get_callbacks():
    ea = EarlyStopping(patience=20)
    return [ea]


n_exps = 100

batch_sizes = [8, 16, 32, 64]
lstm_sizes = [8, 16, 24, 32, 64]
dropouts = [0, 0.1, 0.2, 0.3, 0.4]
lrs = [0.001, 0.0005, 0.0001]
decays = [1e-3, 1e-4]
optimizers = ['adam', 'rmsprop']
activations = ['relu', 'tanh']

confs = list(itertools.product(batch_sizes, lstm_sizes, dropouts, lrs, decays, optimizers, activations))
rn.shuffle(confs)
confs = confs[:n_exps]    


for conf in confs:   
    reset_env()
    
    batch_size = conf[0]
    lstm_size = conf[1]
    dropout = conf[2]
    lr = conf[3]
    decay = conf[4]
    optimizer = conf[5]
    activation = conf[6]
    
    if optimizer == 'adam':
        opt = Adam(lr=lr, decay=decay)
    else:
        opt = RMSprop(lr=lr, decay=decay)
        
    model = get_rnn_model(lstm_size, activation, dropout, opt)
    model.fit(train_seq_x, train_seq_y, validation_data=(valid_seq_x, valid_seq_y), batch_size=batch_size, 
              epochs=1000, verbose=0, shuffle=True, callbacks=get_callbacks())
    
    pred_y = model.predict(valid_seq_x)
    pred_y = y_scaler.inverse_transform(pred_y)
    true_y = y_scaler.inverse_transform(valid_seq_y.reshape(-1, 1))
    
    val_loss = min(model.history.history['val_loss'])
    mae = mean_absolute_error(true_y, pred_y)
    mse = mean_squared_error(true_y, pred_y)
    
    print('MAE: {0}, MSE: {1} val_loss: {2}'.format(mae, mse, val_loss))
    
    result = dict()
    result['back_weeks'] = back_weeks
    result['batch_size'] = batch_size
    result['lstm_size'] = lstm_size
    result['dropout'] = dropout
    result['lr'] = lr
    result['decay'] = decay
    result['optimizer'] = optimizer
    result['mae'] = mae
    result['mse'] = mse
    result['val_loss'] = val_loss
    
    results.append(result)
